package cn.centychen.springboot.starter.xxljob.example.handler;

import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import com.xxl.job.core.log.XxlJobLogger;
import org.springframework.stereotype.Component;

/**
 * Create by cent at 2019-05-10
 */
@JobHandler("demoJobHandler")
@Component
public class DemoJobHandler extends IJobHandler {

    @Override
    public ReturnT<String> execute(String s) throws Exception {
        XxlJobLogger.log("This is a demo job.");
        Thread.sleep(5 * 1000L);
        return SUCCESS;
    }
}
